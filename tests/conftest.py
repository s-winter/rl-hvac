import pytest
import json
from dataclasses import dataclass


@dataclass(frozen=False)
class Config():
    eplus_abs_path: str

    def __init__(self):
        config = json.load(open("config.json"))
        self.eplus_abs_path = config["eplus_abs_path"]

test_config = Config()