'''
Purpose of this module is to check the idf's underlying many of the model variation are working correctly
- The variations themselves are tested in test_env_factory
'''

# Adds stable_baselines3.common.env_checker checking into our testing framework
import pytest
import warnings
from stable_baselines3.common.env_checker import check_env
import numpy as np
import subprocess
import pandas as pd
import numpy as np
from scipy.stats.stats import pearsonr 

from gym_energyplus.envs.energyplus_env import EnergyPlusEnv
from gym_energyplus.wrappers import EarlyEndWrapper, NormaliserWrapper, compute_reward_common

from eval import model_eval
from tests.conftest import test_config as config

########### specific env tests
# these tests should not use the factory as we are interested in testing the rawest form of functionality of the model

# Note the single day variants are not tested here, as they use the same underlying env - just adjust the number of run days
@pytest.mark.parametrize(
    "idf_name,weather_name,wrappers,expected_error_substings", [
        ("basic_model_chilled_water_supply_temp","AUS_NSW.Sydney_IWEC", [],["We recommend you to use a symmetric and normalized Box action space (range=[-1, 1])"]),
        ("basic_model_chilled_water_supply_temp","AUS_NSW.Sydney_IWEC", [NormaliserWrapper],[]),
        ("basic_model_ahu","AUS_NSW.Sydney_IWEC", [],["We recommend you to use a symmetric and normalized Box action space (range=[-1, 1])"]),
        ("basic_model_ahu_supply_air_sp","AUS_NSW.Sydney_IWEC", [],["We recommend you to use a symmetric and normalized Box action space (range=[-1, 1])"])
    ]
)
def test_custom_env_with_check_env(capfd,tmp_path, idf_name, weather_name, wrappers, expected_error_substings):

    '''
    capfd: Pytest fixture. when want to run manual check. pytest hides the stdout, but when debugging want to see and check how far elpus is
    tmp_path: this a fixture that is part of pytest. Creates a path where thing can be written to, and handle clean up
    idf_name: name of the file (excluding .idf) expected to be found in the ./buildings
    weather_name: name of the file (exluding .epw) expected to be found in  ./weather
    env_class: env associated with the idf_name
    wrappers: a list of wrappers to be applied to the env
    expected warnings: the warning that are expected to be generated
    '''

    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:
        for wrapper in wrappers:
            env = wrapper(env)
        
        with warnings.catch_warnings(record= True) as warning_list:
            # - Monitor wrapper prevents early resets. This is required for the testing. montoring is the responsiblity of the learner rather than the env        
            check_env(env, warn = True)

    # Every expected error is found
    for w in warning_list:
        for err_substring in expected_error_substings:
            if err_substring in str(w.message):
                break
        # if a warning can't be matched to any of our expected warnings
        else:
            assert f"unexpected warning generated {str(w.message)}" == False       
    # Ensure no additional errors
    assert len(expected_error_substings) == len(warning_list)    


@pytest.mark.parametrize(
    "idf_name,weather_name", [
        ("basic_model_chilled_water_supply_temp","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu_supply_air_sp","AUS_NSW.Sydney_IWEC")
    ]
)
def test_custom_env_skip_sizing(capfd ,tmp_path, idf_name, weather_name ):
    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:
        env.reset()

    output = capfd.readouterr().out

    assert "Calculating System sizing" in output
    assert "Starting Simulation" in output

@pytest.mark.parametrize(
    "idf_name,weather_name", [
        ("basic_model_chilled_water_supply_temp","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu_supply_air_sp","AUS_NSW.Sydney_IWEC")
    ]
)
def test_custom_env_syd_design_days(capfd ,tmp_path, idf_name, weather_name ):
    '''
    Checks the the design days are for the same env that the weather file is. NOTE: this currently only checks the name of the design days
        - alternative is reading the .idf, which is considered out of scope for now
    '''
    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:
        env.reset()

    output = capfd.readouterr().out
    assert "SYDNEY ANN CLG 1% CONDNS" in output
    assert "SYDNEY ANN HTG 99% CONDNS" in output

@pytest.mark.parametrize(
    "idf_name,weather_name", [
        ("basic_model_chilled_water_supply_temp","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu_supply_air_sp","AUS_NSW.Sydney_IWEC")
    ]
)
def test_custom_env_office_hours_and_no_overnight_reward(tmp_path, idf_name, weather_name ):
    '''
    Running a full simulation for each of these tests individually is time consuming - doing it all in one

    NOTE: The start day, end day and day of week checks in this test are very fragile.
        If the run period is change in the idf these parts of the test will break. If you intentionally change these, delete the lines marked ##run period check##
    '''
    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:

        obs =  env.reset()

        ##start run period check##
        #check the start day
        assert obs[0] == 306, f"start day {obs[0]} is not the agreed start day 306"
        ##end run period check##

        i = 0
        done = False
        while not done and i < 20000:
            # for action space of box, this can be an integer, otherwise should be np.array
            action = np.array([8.4], dtype = "float32")
            last_obs = obs # need to track the last obs, because when done get a fake obs
            obs, rew, done, _ = env.step(action)
            
            if not done:
                ### Perform checks ###
                day_of_year = obs[0]
                time = obs[1]
                assert time >= 7 , obs #start of day (need an action for beginning)
                assert time <= 21 , obs # end of day

                if time == 7:
                    assert rew == 0 , obs # Should recieve no reward for the overnight action
                else: # for every time ecept 7 ensure the step size is correct
                    assert time == last_obs[1] + .25, f"current obs {obs}, last_obs {last_obs}"
                
                ##start run period check##
                # Day of week check
                # This part of the check is highly dependant on the idf. Runtime period doesn't use a real year - you define the day for the first day
                # for basic_model_chilled_water_supply_temp.idf the first (305) is a sunday
                START_INDEX_DAY = 305
                if day_of_year < START_INDEX_DAY:
                    day_of_week = (365 + day_of_year - START_INDEX_DAY) % 7 + 1
                else:
                    day_of_week = (day_of_year - (START_INDEX_DAY)) % 7 + 1# this is 1 based indexing same as energy plus uses
                assert day_of_week not in [1,7], obs 
                ##end run period check##

            i += 1

        assert done == True
        #Check the last day of the year
        day_of_year = last_obs[0]
        ##start run period check##
        assert day_of_year == 91, last_obs
        ##end run period check##

        env.close()


# NOTE: these do not cover single day variants as the only differ in respect to run period.
@pytest.mark.parametrize(
    "idf_name,erl_idf,weather_name,time_action_multi,comparison_vars,tols", [
        ("basic_model_chilled_water_supply_temp","basic_model_chilled_water_supply_temp","AUS_NSW.Sydney_IWEC",[1],['CHILLED WATER LOOP CHW SUPPLY OUTLET:System Node Setpoint Temperature [C](TimeStep)'],[.2]),
        # NOTE: the follow validation point does not pass the test "AHU-EAST SUPPLY FAN OUTLET:System Node Temperature [C](TimeStep)"  
        # -- but looked at the problematic values, there are not too many and they look like similution difference, rather than control not being applied
        ("basic_model_ahu","basic_model_ahu","AUS_NSW.Sydney_IWEC",[1,1/21],['AHU-EAST SUPPLY FAN:Fan Air Mass Flow Rate [kg/s](TimeStep)','AHU-EAST SUPPLY FAN OUTLET:System Node Setpoint Temperature [C](TimeStep)',"AHU-EAST SUPPLY FAN OUTLET:System Node Mass Flow Rate [kg/s](TimeStep)"],[.2,.2,.2]),
        # for the setpoint everything is within .15
        #"AHU-EAST SUPPLY FAN OUTLET:System Node Temperature [C](TimeStep): has two points outside of .2 - looks like minor simulation issue, more important that the setpoint is getting applied properly
        ("basic_model_ahu_supply_air_sp","basic_model_ahu_supply_air_sp","AUS_NSW.Sydney_IWEC",[1],['AHU-EAST SUPPLY FAN OUTLET:System Node Setpoint Temperature [C](TimeStep)',"AHU-EAST SUPPLY FAN OUTLET:System Node Temperature [C](TimeStep)"],[.2, .25]),
    ]
)
def test_custom_env_sp_same_actions_as_erl_sp(tmp_path, idf_name,erl_idf, weather_name,time_action_multi,comparison_vars,tols ):
    '''
    Apply a new action every step. Check that this functions the same as the ERL file for key variables
    Behaviour:
    - run the model using the current time to set each action
    - run the idf that has the equivalent behaviour
    Check that the setpoint values match

    tols: the absolute tolerance for that variable that will be accepted
    '''

    erl_model_file=f"buildings/erl/{erl_idf}.idf"


    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:

        obs =  env.reset()
        time = obs[1]

        i = 0
        done = False
        while not done and i < 20000:
            # for action space of box, this can be an integer, otherwise should be np.array
            actions = [time * action_multi for action_multi in time_action_multi]
            action = np.array(actions, dtype = "float32")
            last_obs = obs # need to track the last obs, because when done get a fake obs
            obs, _, done, _ = env.step(action)
            time = obs[1]
            i += 1 

    #run the basic energy plus file
    cmd = config.eplus_abs_path \
        + ' -r -x' \
        + ' -d ' + str(tmp_path) + "/erl_output" \
        + ' -w ' + f"weather/{weather_name}.epw" \
        + ' ' + erl_model_file
    
    subprocess.run(cmd.split(' '), shell=False, timeout = 120, check = True)

    # sim
    sim = pd.read_csv(str(tmp_path) + "/output/episode-00000000/eplusout.csv.gz")

    # Erl
    erl = pd.read_csv(str(tmp_path) + "/erl_output/eplusout.csv")

    for var, atol in zip(comparison_vars, tols):
        sim_sp = sim[var]
        erl_sp= erl[var]
        # Compare the two files - Need a tolerance becuase the run times seem to be different (worse in ERL)
        # can't work out exactly what is going on - but this should alert us in there are big flat patchs where values are not being applied
        pd.testing.assert_series_equal(sim_sp, erl_sp, atol =atol)


@pytest.mark.parametrize(
    "idf_name,weather_name", [
        ("basic_model_ahu","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu_supply_air_sp","AUS_NSW.Sydney_IWEC")        
    ]
)
def test_ibm_states_match_eso_states(tmp_path, idf_name, weather_name):
    '''
    The IBM tool and Eplus reporting outputs are collected at slightly different points of the simulation. As a result, the state shown to the learner doesn't necessarily match what is recorded in the .eso
    This test checks that these differences are within an acceptable range.
    
    Know issues:
        - power doesn't match exactly ( here might be some additional calc that happen between .eso collection and ibm collects)
    '''

    obs_history = [] # 6224 * 18 - big but not crazy
    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:

        obs =  env.reset()
        obs_history.append(obs)

        i = 0
        done = False
        while not done and i < 20_000:
            # for action space of box, this can be an integer, otherwise should be np.array
            actions = env.action_space.low
            action = np.array(actions, dtype = "float32")
            last_obs = obs # need to track the last obs, because when done get a fake obs
            obs, _, done, _ = env.step(action)

            obs_history.append(obs)
            i += 1 

    learner_obs =pd.DataFrame(obs_history)


    eplus_csv_path = tmp_path / "output" / "episode-00000000" / "eplusout.csv.gz"
    # use the default as there is a bit of filtering required here
    eplus_df= model_eval.get_run_info(str(eplus_csv_path))

    # IBM will contain an entry at 7 - the eso output wont
    filtered_learner = learner_obs[learner_obs[1] != 7.00]
    learner_temps = filtered_learner.iloc[:,[12,13,14,15,16]]
    eplusout_temps = eplus_df.iloc[:,[2,3,4,5,6]]

    assert np.isclose( learner_temps, eplusout_temps).all()

    # We know the power isn't exacly the same - but we should check that it is pretty correlated (e.g similar behaviour in situation, not just in dist)
    eplus_power = eplus_df.iloc[:,-1]
    learner_power = filtered_learner.iloc[:,-1]

    assert pearsonr(eplus_power,learner_power)[0] > .99

@pytest.mark.parametrize(
    "idf_name,weather_name", [
        ("basic_model_ahu_single_day","AUS_NSW.Sydney_IWEC"),
        ("basic_model_ahu_supply_air_sp_single_day","AUS_NSW.Sydney_IWEC")
    ]
)
def test_single_day_variants(tmp_path, idf_name, weather_name):
    '''
    Single day variants are regular idfs that have had thier runtime changed to a single day
    The are expected to match in every other way

    As such the only new functionity that needs testing is:
    - do they start and end at the right time
    '''
    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:

        env = EarlyEndWrapper(env) # requires this wrapper to have proper reset behaviour
        obs =  env.reset()
        
        #check it starts at the right time
        assert obs[1] == 7.0

        #check it ends at the right time

        done = False
        i = 0
        while not (done or i>100):
            actions = env.action_space.low
            action = np.array(actions, dtype = "float32")
            last_obs = obs # need to track the last obs, because when done get a fake obs
            obs, _, done, _ = env.step(action)

            i += 1

        # Need this time to get the reward for the 20:45-21:00 block (21:00 as reported by energyplus)
        assert obs[1] ==21.0

# Only need to test a single control point becuase if one is being applied at the right time the others will be applied at that same time
@pytest.mark.parametrize(
    "idf_name,weather_name,eso_setpoint_names", [
        ("basic_model_ahu_single_day","AUS_NSW.Sydney_IWEC", ["AHU-EAST SUPPLY FAN OUTLET:System Node Setpoint Temperature [C](TimeStep)"]),
        ("basic_model_ahu_supply_air_sp_single_day","AUS_NSW.Sydney_IWEC", ["AHU-EAST SUPPLY FAN OUTLET:System Node Setpoint Temperature [C](TimeStep)"])
    ]
)
def test_start_and_end_day_setpoints_correctly_applied(tmp_path, idf_name, weather_name, eso_setpoint_names):
    '''
    If the time filter at the start and end of the day are incorrecly applied, only a partial amount on the setpoint will come through
    This test double checks the full setpoint is applied at the start and end of the day when time logic is used

    Args:
        eso_setpoint_names: same index order as the action space. The variable in the column to check against
    '''
    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:

        env = EarlyEndWrapper(env) # requires this wrapper to have proper reset behaviour
        _ =  env.reset()

        done = False
        i = 0
        while not (done or i>100):
            actions = env.action_space.high
            action = np.array(actions, dtype = "float32")
            _, _, done, _ = env.step(action)

            i += 1

    # Find the ESO produced and check that the setpoint is actually being set
    #       - could additionally look at info in the observations space - but if we read tho eso to diagnose this problem, thats where we should check it
    df = model_eval.get_run_info(str(tmp_path) + "/output/episode-00000000/eplusout.csv.gz", additional_params = eso_setpoint_names)

    # check that all the value have been set properly in each of the timesteps
    for idx,name in enumerate(eso_setpoint_names):
        set_point_provided = env.action_space.high[idx]
        setpoints_seen = df[name].values
        set_point_provided_list = np.ones(setpoints_seen.shape) * set_point_provided
        assert np.allclose(setpoints_seen,set_point_provided_list, atol = .003 )



@pytest.mark.parametrize(
    "idf_name,weather_name,setpoint_dicts", [
        ("basic_model_chilled_water_supply_temp","AUS_NSW.Sydney_IWEC", [{ 
            "sp":'CHILLED WATER LOOP CHW SUPPLY OUTLET:System Node Setpoint Temperature [C](TimeStep)',
            "res":"CHILLED WATER LOOP CHILLED WATER LOOP:Plant Supply Side Outlet Temperature [C](TimeStep)",
            "atol": 1}, # total range 6-12, now 8 -10 will be our points
        ]),
        ("basic_model_ahu","AUS_NSW.Sydney_IWEC", [{ 
            "sp":'AHU-EAST SUPPLY FAN OUTLET:System Node Setpoint Temperature [C](TimeStep)',
            "res":"AHU-EAST SUPPLY FAN OUTLET:System Node Temperature [C](TimeStep)",
            "atol": 2},{             
            "sp":'AHU-EAST SUPPLY FAN:Fan Air Mass Flow Rate [kg/s](TimeStep)',
            "res":"AHU-EAST SUPPLY FAN OUTLET:System Node Mass Flow Rate [kg/s](TimeStep)",
            "atol": .05}
        ]),
        ("basic_model_ahu_supply_air_sp","AUS_NSW.Sydney_IWEC", [{ 
            "sp":'AHU-EAST SUPPLY FAN OUTLET:System Node Setpoint Temperature [C](TimeStep)',
            "res":"AHU-EAST SUPPLY FAN OUTLET:System Node Temperature [C](TimeStep)",
            "atol": 2}
            ]
        )        
    ]
)
def test_setpoint_lag(tmp_path, idf_name, weather_name,setpoint_dicts):
    '''
    Some of the IDFs have shown system response lagging the control action by a single time step.
    This is problematic, because the effects of the action are not seen until the next step, making it more challenging for the learner
    The fix appears to be changeing AfterPredictorAfterHVACManagers to AfterPredictorBeforeHVACManagers

    Running the two versions above show that very different behaviour (caused by lagging) went from .29 to .004 of the instances
        - repeat an action twice, so half the lagged would be hidden here - likely almost double original control is lagging

        Args:
            sepoint_dict: list of dicts, dicts need follow structure
                sp: the name of the setpoint in the output eso/csv
                res: the name of the response to the sp in the output eso/csv
                atol: the max absolute tolerace of differnce between sp and
    '''

    with EnergyPlusEnv(
        energyplus_file=config.eplus_abs_path,
        model_file=f"buildings/{idf_name}.idf",
        weather_file=f"weather/{weather_name}.epw",
        log_dir=str(tmp_path),
        verbose=False
        ) as env:
        
        low = env.action_space.low
        high = env.action_space.high

        # these multiplies may need too be customised to the variable they related to
        low_cycle = low + (high - low) *.3
        high_cycle = low + (high - low) *.6
        setpoints = [low_cycle,low_cycle,high_cycle,high_cycle]
       

        obs = env.reset()
        done = False
        i = 0
        while not done and i < 20000:
             #Simple cycling control where lag is obvious
            action = setpoints[i% len(setpoints)]
            obs, rewards, done, info = env.step(action)
            i += 1

    # collect the output - and check the sp is always very close (relatively) being met
    additional_params = [setpoint_dict[key] for setpoint_dict in setpoint_dicts for key in ["sp", "res"]]
    df = model_eval.get_run_info(str(tmp_path) + "/output/episode-00000000/eplusout.csv.gz", additional_params = additional_params)

    for setpoint_dict in setpoint_dicts:
        # Not sure the best way to do this tolerance - can't be absolute, because control stuff can be measured in very different way
        # also can't be completely relative because then 0 to 1 is very different to 100 to 101
        # Maybe the easiest way is for this vairable to be passed in
        close = np.isclose(df[setpoint_dict["sp"]], df[setpoint_dict["res"]], atol = setpoint_dict["atol"])
        # There is some know strange behaviour at the beginning of days, and a couple of points in the sim 
        # - What we are trying to catch here is lagging. - when fully swaps to the other control
        assert close.sum() / len(close) > .995