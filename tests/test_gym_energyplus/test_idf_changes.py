"""
There are a number of idfs that are expected to be identical except for a very small number of change

These test ensure these files do no get out of synch
"""
import pytest 
import difflib


@pytest.mark.parametrize(
    "original_path,variant_path,expected_lines", [
        ("buildings/basic_model_ahu.idf","buildings/basic_model_ahu_single_day.idf", [('317', None, '317', None), ('320', None, '320', None), ('2197', None, '2196', '0')]),
        ("buildings/basic_model_ahu_supply_air_sp.idf","buildings/basic_model_ahu_supply_air_sp_single_day.idf", [("317",None,"317",None), ("320",None,"320",None)]),
        ("buildings/basic_model_ahu.idf","buildings/erl/basic_model_ahu.idf", [('2197', None, '2196', '0'), ('4909', '22', '4908', '22')]),
        ("buildings/basic_model_ahu.idf","buildings/basic_model_ahu_supply_air_sp.idf",[('4783', '0', '4784', '5'), ('4824', '5', '4829', '5'), ('4838', None, '4843', None), ('4903', None, '4908', None), ('4930', None, '4934', '0'), ('4937', None, '4940', '0')])
    ]
)
def test_idfs_are_consistent(original_path,variant_path,expected_lines):
    '''
    expected_line = {start_line: number_of_line}
        startline: str, where the change start
        number_of_line: None if single line change, 0 if removed, positive describing the total number of new lines (if more than 1)

    NOTE: the code commented below can help generated the test info, while also checking the diff with vs code
    '''
    with open(variant_path) as variation_fp:
        with open(original_path) as original_fp:
            variation_f = variation_fp.readlines()
            original_f = original_fp.readlines()

    lines = list(filter(lambda x: "@@" in x,difflib.unified_diff(original_f,variation_f, n=0)))

    found_start_lines = []

    import re
    r_pattern = re.compile("(\d+)(,(\d+))?\s.(\d+)(,(\d+))?")

    for item in lines:
        print(item)
        result = r_pattern.search(item)
        
        if result != None:
            first_start = result.group(1)
            first_rows = result.group(3)
            second_start = result.group(4)
            second_rows = result.group(6)
           
            expected_tuple = (first_start, first_rows, second_start,second_rows)
            assert expected_tuple in expected_lines
            found_start_lines.append(expected_tuple)

    assert set(expected_lines) == set(found_start_lines)

# import difflib
# import re

# original_path,variant_path = "buildings/basic_model_ahu.idf","buildings/basic_model_ahu_supply_air_sp.idf"

# with open("../" + variant_path) as variation_fp:
#     with open("../" + original_path) as original_fp:
#         variation_f = variation_fp.readlines()
#         original_f = original_fp.readlines()

# b = list(filter(lambda x: "@@" in x,difflib.unified_diff(original_f,variation_f, n=0)))

# found = [] 

# r_pattern = re.compile("(\d+)(,(\d+))?\s.(\d+)(,(\d+))?")

# for item in b:
#     print(item)
#     result = r_pattern.search(item)
    
#     if result != None:
#         first_start = result.group(1)
#         first_rows = result.group(3)
#         second_start = result.group(4)
#         second_rows = result.group(6)
#         #assert first_start==second_start
#         #assert first_rows==second_rows
        
#         found.append((first_start,first_rows,second_start,second_rows))
# print(found)
# c = list(difflib.unified_diff(original_f,variation_f, n=0))
# c
