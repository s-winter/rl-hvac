import pytest
import os
import glob
from types import SimpleNamespace

from gym_energyplus.envs.energyplus_env import EnergyPlusEnv


def test_clean_up_energyplus_logging(tmp_path):
    # set up the files that should be deleted
    for i in range(0,115):
        path = os.path.join(tmp_path, 'output','episode-{:08}'.format(i),"text.txt")
        os.makedirs(path)

    # Seems like overkill creating an actual class - but mocking is basically suggested to be avoided
    env = EnergyPlusEnv(
                energyplus_file="JUNK",
                # this should not be used, but is required for instantiation not to break
                model_file="basic_model_chilled_water_supply_temp",
                weather_file="JUNK",
                log_dir=str(tmp_path),
                verbose=False)
    env.episode_idx = 114

    # run the clean up
    env.clean_up_logging(keep_x_most_recent = 5, keep_every_x_run = 50)
    # check that only the expected files remain
    remaining = glob.glob( os.path.join(tmp_path,"output","*"))
    remaining_eps = {int(path.split("-")[-1]) for path in remaining}

    assert remaining_eps - set([0,100,109,110,111,112,113,114]) == set([50])