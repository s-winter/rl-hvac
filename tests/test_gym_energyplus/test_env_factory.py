'''
These are integration testing:
They are to ensure what the simulations and wrappers are combined in approapriately.
The bulk of the testing is completed in unit test specific to custom envs or wrappers
'''
import pytest
import gym
from stable_baselines3.common.env_checker import check_env
import numpy as np

from gym_energyplus.env_factory import Factory
@pytest.mark.filterwarnings("ignore: .*Box bound precision lowered by casting to float32")
@pytest.mark.filterwarnings("error")
@pytest.mark.parametrize(
    "env_func_name", [
        ("make_basic_model_no_rew"),
        ("make_basic_ahu"),
        ("make_basic_model_ahu_supply_air_sp")
    ]
)
def test_factory_bounds_and_env_check(tmp_path, env_func_name):
    '''
    Check that passes stable_baseline3 standard env check, and that the bounds listed on the env are normialised
    '''

    factory = Factory()
    env_func = getattr(factory, env_func_name)
    with env_func(str(tmp_path)) as env:

        #Very shallow test - the factory is still very valuable as it standardises this generation process
        assert isinstance(env, gym.Env)
        check_env(env, warn = True)
        #correct bounds on the spaces
        assert (env.observation_space.high == np.ones(env.observation_space.high.shape)).all()
        assert (env.observation_space.low == np.zeros(env.observation_space.low.shape)).all()
        assert (env.action_space.high == np.ones(env.action_space.high.shape)).all()
        assert (env.action_space.low == -1 * np.ones(env.action_space.high.shape)).all()


@pytest.mark.parametrize(
    "env_func_name,reward_lower_bound,reward_upper_bound", [
        ("make_basic_model_no_rew", 0,1),
#        ("make_basic_ahu", -9, 0)  # Multi day calls are not yet supported. Overnight reward needs to be considered
    ]
)
def test_factory_reward_bounds(tmp_path, env_func_name, reward_lower_bound, reward_upper_bound):
    '''
    There is no great way to check the bounds of a reward function
    (e.g Trajectory dependant)

    It is also not well defined what the reward should be bounded to - generally the 0 to 1 or maybe -1 to 1 so that the gradient isn't overwhelmed

    This is a soft sanity test that when  running an env see reward withing your bounds
    '''

    factory = Factory()
    env_func = getattr(factory, env_func_name)
    with env_func(str(tmp_path)) as env:
        env.reset()

        i = 0
        done = False
        non_binary_reward = False # flag that tracks if only 1 or 0 rewards are seen 
        # this would likely mean thats the approariate reward hasn't been applied
        while not done and i < 20000:
            # for action space of box, this can be an integer, otherwise should be np.array
            action = env.action_space.high
            obs , rew, done, _ = env.step(action)
            
            if not done:
                assert rew <= reward_upper_bound, f"Reward ({rew}) was out of bounds [{reward_lower_bound},{reward_upper_bound}]: obs: {obs}, rew: {rew}"
                assert rew >= reward_lower_bound, f"Reward ({rew}) was out of bounds [{reward_lower_bound},{reward_upper_bound}]: obs: {obs}, rew: {rew}"
