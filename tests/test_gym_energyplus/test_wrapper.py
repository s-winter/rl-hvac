import warnings
import pytest
import gym
from stable_baselines3.common.env_checker import check_env
import numpy as np
import math

from gym_energyplus.wrappers import CommonRewardWrapper, NormaliserWrapper, VerifyActionShapeWrapper, compute_reward_common
from gym_energyplus.wrappers_basic_model_ahu import AHUModelObsWrapper

class DummyEnv(gym.Env):
    '''
    Creates a dummy env to be used for testing.
    '''
    def __init__(self):
        super().__init__()
        self.applied_action = None
        self.action_space = gym.spaces.Box(low=6, high=8, shape = (2,), dtype=np.float32)
        self.observation_space = gym.spaces.Box(low=1, high=6, shape = (1,), dtype=np.float32)
        self.metadata = {
            "state_dict" : {"a":0}
        }


    def reset(self):
        self.applied_action =  None
        return np.array([5], dtype = np.float32)

    def step(self, action):
        self.applied_action =action

        obs = np.array([5], dtype = np.float32)
        reward = 1.0
        done = True
        info = {}
        return obs, reward, done, info

##### Normalizer tests #############
def test_normaliser_wrapper_action_space_and_type():
    env = NormaliserWrapper(DummyEnv())

    assert isinstance(env.action_space, gym.spaces.Box)
    assert (env.action_space.high == np.array([1,1],dtype = np.float32)).all()
    assert (env.action_space.low == np.array([-1,-1],dtype = np.float32)).all()


def test_normaliser_wrapper_correct_observation_space():
    env = NormaliserWrapper(DummyEnv())

    assert isinstance(env.observation_space, gym.spaces.Box)
    assert env.observation_space.high == np.array([1],dtype = np.float32) 
    assert env.observation_space.low == np.array([0],dtype = np.float32) 

def test_normaliser_wrapper_step_action_rescaled():
    env = NormaliserWrapper(DummyEnv())
    env.reset() # not strictly required - but standard pattern is to reset before use.
    env.step(np.array([0,1], dtype=np.float32))

    assert (env.env.applied_action == np.array([7,8], dtype=np.float32)).all()

def test_normaliser_wrapper_step_obs_rescaled():
    env = NormaliserWrapper(DummyEnv())
    env.reset() # not strictly required - but standard pattern is to reset before use.
    obs, _, _, _ = env.step(np.array([0,0], dtype=np.float32))

    assert math.isclose(obs[0], .8, abs_tol = 1e-6)

def test_normaliser_wrapper_step_obs_not_box():
    env = DummyEnv()
    env.action_space = gym.spaces.Discrete(3)

    with pytest.raises(AssertionError):
        NormaliserWrapper(env)

def test_normaliser_wrapper_step_action_not_box():
    env = DummyEnv()
    env.observation_space = gym.spaces.Discrete(3)

    with pytest.raises(AssertionError):
        NormaliserWrapper(env)

def test_normaliser_wrapper_dummy_env_check():
    env = NormaliserWrapper(DummyEnv())
    with warnings.catch_warnings(record= True) as warning_list:
        check_env(env, warn = True)

    assert not warning_list, [str(w.message) for w in warning_list]

########## action verifier test ###############
@pytest.mark.parametrize(
    "actions", [
        ([7,8]), #actions within bounds
        ([0,1]) ,# actions outside of bound - this is perfectly acceptable for this wrapper
    ]
)
def test_verify_action_shape_wrapper_correct_action(actions):
    env = VerifyActionShapeWrapper(DummyEnv())
    env.reset() # not strictly required - but standard pattern is to reset before use.
    env.step(np.array(actions, dtype=np.float32))

    assert (env.env.applied_action == np.array(actions, dtype=np.float32)).all()

def test_verify_action_shape_wrapper_incorrect_action():
    env = VerifyActionShapeWrapper(DummyEnv())
    env.reset() # not strictly required - but standard pattern is to reset before use.
    with pytest.raises(gym.error.InvalidAction):
        env.step(np.array([7], dtype=np.float32))

def test_verify_action_shape_wrapper_incorrect_action_space():
    env = DummyEnv()
    env.action_space = gym.spaces.Discrete(3)

    with pytest.raises(AssertionError):
        VerifyActionShapeWrapper(env)


############# Common reward tests #############
@pytest.mark.parametrize( 
    "temps,powers,additional_params,expected_rew",[
        ([22.5,22.5,22.5],[0], {"temperature_gaussian_weight": 1}, 1),
        ([22.5,22.5,22.5],[0], {"temperature_gaussian_weight": 1}, 1), # can shift temp location
        ([23],[0],{"temperature_gaussian_weight": 1},.77880), # Follow their non-standard gaus
        ([23,22.5],[0],{"temperature_gaussian_weight": 1}, .8894), # Normalisation is occuring, so scores for different number temps compariable
        ([25],[0],{"temperature_trapezoid_weight": 1},-2), #check the trapazoid component
        ([25,24],[0],{"temperature_trapezoid_weight": 1},-1.5), #check the trapazoid component normalisation
    ]
)
def test_common_reward(temps,powers,additional_params,expected_rew):

    rew, _ = compute_reward_common(temps,powers,**additional_params)
    
    assert math.isclose(rew, expected_rew, abs_tol = 1e-5)

####################### Rewards tests ####################
# NOTE: testing the specific wrapper of this does not hold a lot of value
#  - would need to make sure the keys are the same, and the keys on the env are in the right spot
# - and that when created state_dict ends up in the metadata
def test_common_reward_wrapper_env_not_normalised():
    env = DummyEnv()
    env.observation_space = gym.spaces.Box(low=0, high=1, shape = (3,), dtype=np.float32)

    with pytest.raises(AssertionError):
        CommonRewardWrapper(env)


def make_dummy_rew_wrapper(temp_keys, power_keys):
    class DummyRewWrapper(CommonRewardWrapper):
        
        @staticmethod
        def get_reward_params():
            # these are default values - not being tested
            return {
                "temperature_center": 22.5,
                "temperature_tolerance": 1.5,
                "temperature_gaussian_weight": 0.1,
                "temperature_gaussian_sharpness": 0,
                "temperature_trapezoid_weight": 0.3,
                "hvac_power_weight": 0.0005
            }
        @staticmethod
        def get_temp_keys() -> list:
            return temp_keys
        @staticmethod
        def get_power_keys() -> list:
            return power_keys

    return DummyRewWrapper

@pytest.mark.parametrize(
    "obs,temp_keys,power_keys,state_dict,expected_rew", [
        ([8,22.5,22.5,3], ["tk1","tk2"], ["power_key_name"], dict(CurrentTime = 0 ,tk1= 1,tk2 =2,power_key_name = 3), 0.0985),
        ([8,22.5,22.5,4,3], ["tk1","tk2"], ["power_key_name"], dict(CurrentTime = 0, tk1= 1,tk2 =2,junk_key = 3, power_key_name = 4), 0.0985), #Extra key
        ([8,22.5,3], ["tk1"], ["power_key_name"], dict(CurrentTime = 0, tk1= 1, power_key_name = 2),  0.0985), # differnt number of keys
        ([8,3,22.5,22.5], ["tk1","tk2"], ["power_key_name"], dict(CurrentTime = 0,tk1= 3,tk2 =2,power_key_name = 1),  0.0985), # different ordering of keys
    ]
)
def test_reward_wrapper(obs, temp_keys, power_keys, state_dict, expected_rew):
    # Assume that working with an env that has the correct parts - check the basic functionlity works
    # base env expected to have a state_dict - this is present in dummy env
    env = DummyEnv()
    env.metadata["state_dict"] = state_dict

    # Make a test class that instatiates the correct parts
    wrapped_env = make_dummy_rew_wrapper(temp_keys, power_keys)(env)

    actual_rew = wrapped_env.reward(obs)
    
    assert math.isclose(expected_rew, actual_rew)
    metadata_added = wrapped_env.metadata["DummyRewWrapper"]

    assert metadata_added["temp_keys"] == temp_keys
    assert metadata_added["power_keys"] == power_keys
    assert metadata_added["reward_params"] == wrapped_env.get_reward_params()

@pytest.mark.parametrize(
    "times_to_exclude", [6,7,21.25,22]
)
def test_reward_wrapper_7am(times_to_exclude):
    env = DummyEnv()
    env.metadata["state_dict"] = {"CurrentTime":0}

    # Make a test class that instatiates the correct parts
    wrapped_env = make_dummy_rew_wrapper([1], [1])(env)

    with pytest.raises((NotImplementedError, ValueError)):
        wrapped_env.reward([times_to_exclude])

def test_reward_wrapper_state_dict_and_obs_dont_match():
    env = DummyEnv()
    env.metadata["state_dict"] = {"CurrentTime":0}

    # Make a test class that instatiates the correct parts
    wrapped_env = make_dummy_rew_wrapper([1], [1])(env)

    with pytest.raises( ValueError):
        wrapped_env.reward([8,6])

def test_reward_wrapper_state_dict_missing_temp_or_power():
    env = DummyEnv()
    env.metadata["state_dict"] = {"CurrentTime":0}

    # Make a test class that instatiates the correct parts
    with pytest.raises(AssertionError):
        make_dummy_rew_wrapper([1], [])(env)
    
    with pytest.raises(AssertionError):
        make_dummy_rew_wrapper([], [1])(env)     


################# OBs wrapper tests #################
@pytest.mark.parametrize(
    "wrapper,full_obs,filtered_obs", [
        (AHUModelObsWrapper,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], [2,3,11,12,14,15,19]),
    ]
)
def test_obs_wrapper(wrapper,full_obs,filtered_obs):
    # Created dummy env that will return the full obs, so we can then filter it
    dummy_obs = np.array(full_obs)

    def step_override(action):
        return dummy_obs, None, None, {}

    env = DummyEnv()
    env.observation_space  = gym.spaces.Box(low=dummy_obs, high=dummy_obs, dtype=np.float32)
    env.step = step_override

    # Wrap and filter the obs
    env = wrapper(env)
    
    obs, _, _, _ =env.step(None)
    assert obs.tolist() == filtered_obs
    
    # Check the oibservation space has also been changed correctly
    assert env.observation_space.low.tolist() == filtered_obs
    assert env.observation_space.low.tolist() == filtered_obs

