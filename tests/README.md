The test suite can be run by calling: 
`pytest .`

These tests provide a guide of what to evaluate when extending this frame work to new building model. `test_check_custom_env.py` may be of particular interest.