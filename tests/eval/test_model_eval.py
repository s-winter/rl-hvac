import pytest
from eval import model_eval
from eval.model_eval import *

@pytest.fixture
def dummy_run_data_path():
    return "tests/eval/dummy_data/eplusout.csv.gz"

# test can read a file with standard columns names
def test_get_run_info_default_columns(dummy_run_data_path):
    
    df = get_run_info(dummy_run_data_path)
    assert df.columns.tolist() == [
        'Date/Time',
        'Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)',
        'SPACE1-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE2-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE3-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE4-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE5-1:Zone Mean Air Temperature [C](TimeStep)',
        'Whole Building:Facility Total HVAC Electric Demand Power [W](TimeStep)'
    ]

def make_dummy_eplusout_df(filtered = False):
    '''
    The minimal eplusout file - NOTE: the timesteps do not match exactly what would expect to find
    Args:
        filtered: if True, will return a result similar to get_run_info - containing only rows with power
    '''
    dummy_data = [[' 11/01  00:15:00', 1,2,3,4,5,6,0],
                [' 11/01  01:15:00', 1,2,3,4,5,6,0],
                [' 11/01  07:00:00', 1,2,3,4,5,6,0],
                [' 11/01  07:15:00', 1,2,3,4,5,6,100],
                *([[' 11/01  08:15:00', 1,2,3,4,5,6,100]] * 54),
                [' 11/01  20:15:00', 1,2,3,4,5,6,100],
                [' 11/01  21:15:00', 1,2,3,4,5,6,0],
                [' 11/02  00:15:00', 1,2,3,4,5,6,0],
                [' 11/02  01:15:00', 1,2,3,4,5,6,0],
                [' 11/02  07:00:00', 1,2,3,4,5,6,0],
                [' 11/02  07:15:00', 1,2,3,4,5,6,100],
                *([[' 11/02  08:15:00', 1,2,3,4,5,6,100]] * 54),
                [' 11/02  17:15:00', 1,2,3,4,5,6,100],
                [' 11/02  21:15:00', 1,2,3,4,5,6,0],
            ]
    cols = [
        'Date/Time',
        'Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)',
        'SPACE1-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE2-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE3-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE4-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE5-1:Zone Mean Air Temperature [C](TimeStep)',
        'Whole Building:Facility Total HVAC Electric Demand Power [W](TimeStep)'
    ]
    df = pd.DataFrame(dummy_data, columns= cols)

    if filtered:
        return df.loc[df['Whole Building:Facility Total HVAC Electric Demand Power [W](TimeStep)'] != 0,:]

    return  df

def test_get_run_info_appropriate_filtering_power(tmp_path):
    '''
    There are some know issues with functionality.

    Model runs for 6213 time step, filtered output is 6112

    Of that difference:
    filtered output missing 109 times at 7am when no power is used
        - this is acceptable because 7am represents the END of the time step. (e.g 6:45 to 7). 
        - During this time step we are interested in the obs, and action we choose, but the reward related to the previous action that was chosen
            - as this is primary used to look at performance, don't care about the performance outside out time window
    
    Some power usage on non work days (8 additonal steps):
        - When does this happen?  2 of jan 16:1temps5:00, (4 steps)  03/28 15:15:00 (4 steps)
        - how does this usage compare to a standard day (normal usage?): similar level
        - Does it also happen to the baseline: Yes
        - Is our learner on or off during this period: Is using the default values (usually for system off)
            - how signisficant is this going to be on performance in comparison to the actual baseline
                - 8 steps at setpoint 10 (on average this uses 7120 watts per step)

        NOTE: these values are now filtered out - to make it more comparable with what the model actually sees
            - This is important once we start runnign the reward back over the eplus output.

    filter output steps + missing morning - additional steps(now removed)
    6112                +    109          -     0 (now removed)               = 6213

    '''

    dummy_df = make_dummy_eplusout_df()
    # Save this to the tmp_path so it can then be read in
    path = tmp_path / "dummy_eplusout.csv"
    dummy_df.to_csv(path)

    # expect there to be no warning thrown and end up with the following
    df = get_run_info(path)
    # expected output
    expected_data = [
                  [' 11/01  07:15:00', 1,2,3,4,5,6,100],
                  *([[' 11/01  08:15:00', 1,2,3,4,5,6,100]] * 54),
                  [' 11/01  20:15:00', 1,2,3,4,5,6,100],
                  [' 11/02  07:15:00', 1,2,3,4,5,6,100],
                  *([[' 11/02  08:15:00', 1,2,3,4,5,6,100]] * 54),
                  [' 11/02  17:15:00', 1,2,3,4,5,6,100],
                ]

    assert df.values.tolist() == expected_data

def test_get_run_info_appropriate_filtering_missing_data(tmp_path):
    '''
    When there is an incorrect number of entries check that remove these days and present a warning
    '''

    dummy_df = make_dummy_eplusout_df()
    # drop rows from the first day
    dummy_df.drop(index = [10,11,12,13,14,15],inplace = True)
    # Save this to the tmp_path so it can then be read in
    path = tmp_path / "dummy_eplusout.csv"
    dummy_df.to_csv(path)

    # expect there to be no warning thrown and end up with the following
    with pytest.warns(UserWarning):
        df = get_run_info(path)
    # expected output
    expected_data = [
                  [' 11/02  07:15:00', 1,2,3,4,5,6,100],
                  *([[' 11/02  08:15:00', 1,2,3,4,5,6,100]] * 54),
                  [' 11/02  17:15:00', 1,2,3,4,5,6,100]
    ]

    assert df.values.tolist() == expected_data


# check can add aditional columns
def test_get_run_info_additional_columns(dummy_run_data_path):

    new_cols = ["CHILLED WATER LOOP CHILLED WATER LOOP:Plant Supply Side Outlet Temperature [C](TimeStep)"]

    df = get_run_info(dummy_run_data_path, new_cols)

    assert set(new_cols) - set(df.columns) == set()

# check error message if pass a bad column name
def test_get_run_info_invalid_additional_column(dummy_run_data_path):
    new_cols = ["non_existant_col_name"]

    with pytest.raises(AssertionError):
        df = get_run_info(dummy_run_data_path, new_cols)

def test_additional_metrics():
    test_df = pd.DataFrame({"Mean Air Temperature1": [21,22,23,24], "Mean Air Temperature": [25,26,27,28]})
    metrics_df = model_eval.additional_metrics(test_df)

    expected_df = pd.DataFrame({
            "Mean Air Temperature1": [4.0, 22.5, 1.29, 21.0, 21.75, 22.5, 23.25, 24.0, 0.0, 0.0, 0.0],
            "Mean Air Temperature": [ 4.0, 26.5, 1.29, 25.0, 25.75, 26.5, 27.25, 28.0, 4.0, 10.0, 30.0]
            },
        index = ['count', 'mean', 'std', 'min', '25%', '50%', '75%', 'max', "count_above_24", "above_24_sum", "above_24_sq_sum"]
        )

    pd.testing.assert_frame_equal(metrics_df, expected_df, atol = .01)



### Reward_calc
def dummy_series():
    dummy_data = [' 11/01  07:15:00', 1,2,3,4,5,6,100]
    cols = [
        'Date/Time',
        'Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)',
        'SPACE1-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE2-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE3-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE4-1:Zone Mean Air Temperature [C](TimeStep)',
        'SPACE5-1:Zone Mean Air Temperature [C](TimeStep)',
        'Whole Building:Facility Total HVAC Electric Demand Power [W](TimeStep)'
    ]
    return pd.Series(dummy_data, index= cols)

def test_reward_calc():
    metadata = {
        "reward_params": {
            "temperature_center": 22.5,
            "temperature_tolerance": 1.5,
            "temperature_gaussian_weight": 0.1,
            "temperature_gaussian_sharpness": 0,
            "temperature_trapezoid_weight": 0.3,
            "hvac_power_weight": 0.0005
        },
        "temp_keys": ["t2","t3"],
        "power_keys": ["HVACpower"],
    }
    series = dummy_series()

    actual_score = reward_calc(series, metadata)
    expected_score =  wrappers.compute_reward_common([4,3], power = [100],**metadata["reward_params"])[0]
    assert actual_score==expected_score

# Different naming has been used by env than t1,t2,t3,t4
def test_reward_calc_non_standard_temp_keys():
    metadata = {
        "reward_params": {
            "temperature_center": 22.5,
            "temperature_tolerance": 1.5,
            "temperature_gaussian_weight": 0.1,
            "temperature_gaussian_sharpness": 0,
            "temperature_trapezoid_weight": 0.3,
            "hvac_power_weight": 0.0005
        },
        "temp_keys": ["Wrongkey","Wrongkey2"], #the thing being tested
        "power_keys": ["HVACpower"],
    }
    series = dummy_series()

    with pytest.raises(KeyError) as ex_info:
        reward_calc(series, metadata)

    assert "Wrongkey" in ex_info.value.args[0]