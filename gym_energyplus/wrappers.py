import gym
import numpy as np


class ObsRewardWrapper(gym.Wrapper):
    
    def __init__(self, env):
        super().__init__(env)

    def reset(self, **kwargs):
        return self.env.reset(**kwargs)

    def step(self, action):
        observation, _, done, info = self.env.step(action)
        return observation, self.reward(observation), done, info

    def reward(self, observation):
        raise NotImplementedError

class ObservationFilterWrapper(gym.ObservationWrapper):
    '''
    Filter an obsevation space (using filter method), creating a new observation space with just those values
    (The original info is still available in the wrapped Env)
    '''
    def __init__(self, env):
        super().__init__(env)
    
        low = self.env.observation_space.low[self.filter() ]
        high = self.env.observation_space.high[self.filter() ]

        self.observation_space = gym.spaces.Box(low=low, high=high, dtype=np.float32)
        _keys_to_keep = [val for i, val in enumerate(self.metadata["state_dict"].keys()) if self.filter()[i]] 
        state_dict = { item:idx for idx,item in enumerate(_keys_to_keep)}

        # This is the concrete subclass
        child_class = self.__class__
        self.metadata.update({
            "state_dict": state_dict,
            child_class.__name__ : {
                "observation_space": { 
                        "low":  self.observation_space.low.tolist(),
                        "high":  self.observation_space.high.tolist(),
                        "state_dict": state_dict,
                    },
                "filter" : child_class.filter()
            }
        })
    
    def observation(self, observation):
        return observation[self.filter()]  

    @staticmethod
    def filter():
        '''
        Provide an array of boolean values determine which values of the original observation are kept
        '''
        raise NotImplementedError


class CommonRewardWrapper(ObsRewardWrapper):
    '''
    Wrapper for using the compute_reward_common reward function and tracing the parameters use
    '''
    
    def __init__(self, env):

        super().__init__(env)
        # brief check that it doesn't apprear to be normalised already (would be an easy mistake to make)
        assert not (np.all(env.observation_space.high == 1) or np.all(env.observation_space.low == 0)), \
                "appears env has been normalized, reward function expects raw input"    

        # This is the concrete subclass
        child_class = self.__class__
        assert child_class.get_temp_keys() != []   #if not using temp, should be adjust reward params
        assert child_class.get_power_keys() != []  #if not using power, should be adjust reward params

        self.state_dict = self.metadata["state_dict"].copy()


        self.metadata.update({
            child_class.__name__: {
                "reward_params": child_class.get_reward_params(),
                "temp_keys": child_class.get_temp_keys(),
                "power_keys": child_class.get_power_keys(),
                "state_dict_used": self.state_dict,
            }
        })

    def reward(self, obs):
        # Over night no rewards should be recieved 
        time = obs[self.state_dict["CurrentTime"]]
        if time <6.95 or time>21.05:
            raise ValueError(f"only times within operating hour are expected. recieved; {time}")
        if len(self.state_dict) != len(obs):
            raise ValueError(f"state_dict has different number of entries to obs. state_dict {self.state_dict}, obs {list(obs)}")
        elif obs[self.state_dict["CurrentTime"]] == 7:
            # it is unclear what the reward should be overnight (Can't just make it zero because if all other rewards are negative this then highly rewards the behaviour)
            # TODO: need to adress this when moving back to multi day simulations - Should only use day long eps because there is no elegant way of handling the overnight effect
            raise NotImplementedError("This function does not support multiday calls")
        else:
            child_class = self.__class__ # This is the concrete subcalss
            temps = [obs[self.state_dict[temp_key]] for temp_key in child_class.get_temp_keys()]
            powers = [obs[self.state_dict[pow_key]] for pow_key in child_class.get_power_keys()]
            rew =   compute_reward_common(temps = temps, power = powers, **child_class.get_reward_params())[0]

        return rew

    @staticmethod
    def get_reward_params():
        """
        Dictionary of parameter that need to be passed to compute_reward_common.
        """
        raise NotImplementedError

    @staticmethod
    def get_temp_keys() -> list:
        """
        returns a list of names that are used to select approapirate parts of the state
        The keys that can be used here are defined on the original energyplus env, and are in the self.metadata.state_dict
        """
        raise NotImplementedError

    @staticmethod
    def get_power_keys() -> list:
        """
        returns a list of names that are used to select approapirate parts of the state
        The keys that can be used here are defined on the original energyplus env, and are in the self.metadata.state_dict
        """
        raise NotImplementedError


class NormaliserWrapper(gym.Wrapper):
  """
    Rescale the observation to be between [0,1] (adjusts output)
    Rescales the actions to be between  [-1,1] (adjusts input action)

  :param env: (gym.Env) Gym environment that will be wrapped
  """
  def __init__(self, env):
    assert isinstance(env.action_space, gym.spaces.Box), "This wrapper only works with continuous action space (spaces.Box)"
    assert isinstance(env.observation_space, gym.spaces.Box), "This wrapper only works with continuous observation space (spaces.Box)"
    
    super(NormaliserWrapper, self).__init__(env)
    # Override at action and observation space (The original info is still available in Env)
    self.action_space = gym.spaces.Box(low=-1, high=1, shape= env.action_space.shape, dtype=np.float32)
    self.observation_space = gym.spaces.Box(low=0, high=1, shape= env.observation_space.shape, dtype=np.float32)

    self.metadata.update({
        "NormaliserWrapper": {
            "observation_space": { 
                    "low":  self.observation_space.low.tolist(),
                    "high":  self.observation_space.high.tolist()
                },
            "action_space": { 
                    "low":  self.action_space.low.tolist(),
                    "high":  self.action_space.high.tolist()
                }
        }
    })
 

  def rescale_action(self, scaled_action):
    """
    Rescale the action from [-1, 1] to [low, high]
    (no need for symmetric action space)test_normaliser_wrapper_step_obs_rescaled
    """
    return self.env.action_space.low + (0.5 * (scaled_action + 1.0) * (self.env.action_space.high -  self.env.action_space.low))

  def rescale_obs(self, unscaled_obs):
    """
    Rescale the action from [low, high] t0 [0,1]
    :param unscaled_obs: (np.ndarray)
    :return: (np.ndarray)
    """
    return (unscaled_obs - self.env.observation_space.low) / (self.env.observation_space.high - self.env.observation_space.low)

  def reset(self):
    """
    Reset the environment 
    """
    return self.rescale_obs(self.env.reset())

  def step(self, action):
    """
    :param action: ([float] or int) Action taken by the agent
    :return: (np.ndarray, float, bool, dict) observation, reward, is the episode over?, additional informations
    """
    # Rescale action from [-1, 1] to original [low, high] interval
    rescaled_action = self.rescale_action(action)
    obs, reward, done, info = self.env.step(rescaled_action)
    return self.rescale_obs(obs), reward, done, info


class VerifyActionShapeWrapper(gym.ActionWrapper):
  '''
  Ensure input action are of the correct size
  '''
  def __init__(self, env):
    assert isinstance(env.action_space, gym.spaces.Box), "This wrapper only works with continuous action space (spaces.Box)"
    super().__init__(env)

  def action(self, act: np.ndarray):

    if  act.shape != self.env.action_space.shape:
      raise gym.error.InvalidAction(f"recieved actions of {act.shape}, expected {self.env.action_space.shape}")
    else:
      return act



class EarlyEndWrapper(gym.Wrapper):
    '''
    This wrapper means only the first day of the simulation is run
    '''
    def __init__(self, env):
        super().__init__(env)
        
        self.metadata.update({
            "SingleDayWrapper" : {
                "early_termination_condition": "time >= 21"
            }
        })

    def step(self, action):

        obs, rew, done, info = self.env.step(action)
        
        time = obs[1]
        if time >= 21:
            done = True
            # Now need to make sure the undelying sim gets finished
            # ESO is not identically in time with sim, don't want to miss and of the info
            _finish_sim(self.env)
        
        return  obs, rew, done, info

def _finish_sim(env):
    '''
    Run an env that has already been instantiated until it hits done (or times out)
    '''
    done = False
    i = 0
    while not (done or i>20_000):
        actions = env.action_space.low
        action = np.array(actions, dtype = "float32")
        _ , _, done, _ = env.step(action)

        i += 1

    if i>20_000:
        raise TimeoutError("While loop has timed out")

def compute_reward_common(
                              temps,
                              power,  
                              temperature_center = 22.5,
                              temperature_tolerance = 0.5,
                              temperature_gaussian_weight = 0.,
                              temperature_gaussian_sharpness = 1.,
                              temperature_trapezoid_weight = 0.,
                              hvac_power_weight = 0
                              ):

        ''' 
        This function serves as the basis for reward calcuations, follow the example by IBM.
        Key changes:
         - The gausian the temprerature component is normalised by the number of zones 
         - NOTE: the gaussian is not a stric PDF (e.g won't intergrate to 1 because the constants have been removed)
         - is a lot fatter 

        New arguements:
            temp_index (list[int]): list of temps that should be included in the reward
            power_index (list[int]): list of power values that should be included in the reward. Currenlt only supports 1 item
        '''
        
        assert len(power) == 1

        hvac_power = power[0]
        
        # Temp. gaussian
        rew_temps_gaus = [np.exp(-(temp - temperature_center) * (temp - temperature_center) * temperature_gaussian_sharpness) * temperature_gaussian_weight for temp in temps]
        rew_temp_gaussian = sum(rew_temps_gaus)/len(temps)
        # Temp. Trapezoid
        phi_low = temperature_center - temperature_tolerance
        phi_high = temperature_center + temperature_tolerance

        rew_temp_trapezoids = []
        for temp in temps:
            if temp < phi_low:
                rew_temp_trapezoid = - temperature_trapezoid_weight * (phi_low - temp)
            elif temp > phi_high:
                rew_temp_trapezoid = - temperature_trapezoid_weight * (temp - phi_high)
            else:
                rew_temp_trapezoid = 0.
            rew_temp_trapezoids.append(rew_temp_trapezoid)

        rew_temp_trapezoid = sum(rew_temp_trapezoids) / len(temps)
        
        rew_fluct = 0.

        rew_hvac_power = - hvac_power * hvac_power_weight
        rew =  rew_temp_gaussian +  rew_temp_trapezoid + rew_fluct  + rew_hvac_power

        return rew, (rew_temp_gaussian, rew_temp_trapezoid, rew_fluct, rew_hvac_power)