This module is used to create gym environments of HVAC simulations. Key files are:

| Name | Description |
| ---  | ----------- |
|env|The primary environments that interact with the different building models. This are added when changing which components are under EnerygPlus control|
|wrappers|A library of gym wrappers. These are used to change the state, reward, and reset behaviour of an environment.|
|env_factory| This combines the envs and wrappers to produce environments that are suitable for exploration and use.|