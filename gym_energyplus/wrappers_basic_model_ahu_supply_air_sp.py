"""
This file contains all the wrappers that pertain to basic_model_ahu
"""

from gym_energyplus.wrappers import  ObservationFilterWrapper


class AHUModelTempOnlyObsWrapper(ObservationFilterWrapper):
    @staticmethod
    def filter():
        return [
                False, # DayOfYear 
                True, # CurrentTime 
                True, # OutdoorTemp 
                False, # OutdoorRelHum 
                False, # OutdoorDewTemp 
                False, # OutdoorTempWB 
                False, # OutdoorDiffuseSolar 
                False, # OutdoorDirectSolar 
                False, # OutdoorAzimuthAngle 
                False, # OutdoorAltitudeAngle 
                False, # AHU_east_temp
                False, # AHU_east_fan
                False, # t1 
                True, # t2 
                True, # t3 
                False, # t4 
                False, # t5 
                False, # ChilledWaterSupplyTemp 
                False, # hvac_power  
            ]   