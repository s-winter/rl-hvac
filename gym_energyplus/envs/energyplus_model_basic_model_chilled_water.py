import os
import time
import numpy as np
import pandas as pd
import datetime as dt
from gym import spaces
from gym_energyplus.envs.energyplus_model import EnergyPlusModel
from glob import glob
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.widgets import Slider, Button, RadioButtons

class EnergyPlusModelBasicModelChilledWater(EnergyPlusModel):

    def __init__(self,
                 model_file,
                 log_dir,
                 verbose=False):
                 
        super(EnergyPlusModelBasicModelChilledWater, self).__init__(model_file, log_dir, verbose)

        
    def setup_spaces(self):
        # Bound action temperature
        self.action_space = spaces.Box(low =   np.array([ 6]),
                                       high =  np.array([ 12]),
                                       dtype = np.float32)


        # NOTE: the names are used to extract the correct info out of this function. There is no global naming convention for these. These will be used by the function the interact with this env.
        name_min_max = [
        ("DayOfYear",               1 ,  365),
        ("CurrentTime",             7 ,  21), 
        ("OutdoorTemp",             -20, 50) ,
        ("OutdoorRelHum",           0, 100) , 
        ("OutdoorDewTemp",          -5, 30) , 
        ("OutdoorTempWB",           0, 40)  , 
        ("OutdoorDiffuseSolar",     0, 500) , 
        ("OutdoorDirectSolar",      0, 800) , 
        ("OutdoorAzimuthAngle",     0, 360) , 
        ("OutdoorAltitudeAngle",    -80, 80), 
        #TODO: model_eval.reward_calc relies on the t1 t2 t3 t4 t5 naming convention of the temp. This is not great, and should be fixed in future
        ("t1",                      -20, 50),        
        ("t2",                      -20, 50),        
        ("t3",                      -20, 50),        
        ("t4",                      -20, 50),        
        ("t5",                      -20, 50),        
        ("ChilledWaterSupplyTemp",  5, 15)  ,        
        ("HVACpower",               0, 30_000) 
        ]                       
        self.observation_space = spaces.Box(low =   np.array([min_val for _,min_val, _ in name_min_max]),
                                            high =  np.array([max_val for _,_, max_val in name_min_max]),
                                            dtype = np.float32)
        self.state_dict = {name:i for i,(name,_,_) in enumerate(name_min_max)}
 
    def set_action(self, action):
        '''
        This override the standard overide in the parent class.
        This was previously used to check/adjust actions. This should be done with wrapper instead (make it more obvious)
        '''
        self.action = action


    # All abstract methods that require implementing
    def compute_reward(self):
        time_of_day = self.raw_state[1] # this attribute is set and updated by energyplus_env

        if time_of_day == 7: # reward here would be to do with overnight - we don't want this
            return 0
        else:
            return 1

    def read_episode(self, ep):
        raise(NotImplementedError)

    def plot_episode(self, ep):
        raise(NotImplementedError)

    # Need to handle the case that raw_state is None
    def set_raw_state(self, raw_state):
        if raw_state is not None:
            self.raw_state = raw_state
        else:
            self.raw_state = self.observation_space.low.tolist()

    def format_state(self, raw_state):
        return np.array([*raw_state])

    def read_episode(self, ep):
        raise(NotImplementedError)

    def plot_episode(self, ep):
        raise(NotImplementedError)

    def dump_timesteps(self, log_dir='', csv_file='', **kwargs):
        raise(NotImplementedError)

    def dump_episodes(self, log_dir='', csv_file='', **kwargs): 
        raise(NotImplementedError)