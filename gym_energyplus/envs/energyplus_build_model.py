# Copyright (c) IBM Corp. 2018. All Rights Reserved.
# Project name: Reinforcement Learning Testbed for Power Consumption Optimization
# This project is licensed under the MIT License, see LICENSE

from re import match
import os

from gym_energyplus.envs.energyplus_model_basic_model_chilled_water import EnergyPlusModelBasicModelChilledWater
from gym_energyplus.envs.energyplus_model_basic_model_ahu import EnergyPlusModelBasicModelAHU
from gym_energyplus.envs.energyplus_model_basic_model_ahu_supply_air_sp import EnergyPlusModelBasicModelAHUSupplyAirSP


#TODO: the matching here is bad, things need to be listed from most specific to least specific
def build_ep_model(model_file, log_dir, verbose = False):
    model_basename = os.path.splitext(os.path.basename(model_file))[0]

    
    if match('basic_model_ahu_supply_air_sp.*', model_basename):
        model = EnergyPlusModelBasicModelAHUSupplyAirSP(model_file = model_file, log_dir = log_dir, verbose = verbose)
    elif match('basic_model_ahu.*', model_basename):
        model = EnergyPlusModelBasicModelAHU(model_file = model_file, log_dir = log_dir, verbose = verbose)
    elif match('basic_model.*', model_basename):
        model = EnergyPlusModelBasicModelChilledWater(model_file = model_file, log_dir = log_dir, verbose = verbose)
    else:
        raise ValueError('Unsupported EnergyPlus model')
    return model
