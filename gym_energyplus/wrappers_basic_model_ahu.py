"""
This file contains all the wrappers that pertain to basic_model_ahu
"""
import gym
import numpy as np

from gym_energyplus.wrappers import CommonRewardWrapper, ObservationFilterWrapper

class AHUModelRewardWrapper(CommonRewardWrapper):
    
    @staticmethod
    def get_reward_params(): 
        return   {
            "temperature_center": 23.5,
            "temperature_tolerance": .5,
            "temperature_gaussian_weight": 0.1,
            "temperature_gaussian_sharpness": 0,
            "temperature_trapezoid_weight": 0.3,
            "hvac_power_weight": 0.0005
        }
    
    @staticmethod
    def get_temp_keys():
        return ["t2","t3"]        

    @staticmethod
    def get_power_keys():
        return ["HVACpower"]        

class AHUModelObsWrapper(ObservationFilterWrapper):
    @staticmethod
    def filter():
        return [
                False, # DayOfYear 
                True, # CurrentTime 
                True, # OutdoorTemp 
                False, # OutdoorRelHum 
                False, # OutdoorDewTemp 
                False, # OutdoorTempWB 
                False, # OutdoorDiffuseSolar 
                False, # OutdoorDirectSolar 
                False, # OutdoorAzimuthAngle 
                False, # OutdoorAltitudeAngle 
                True, # AHU_east_temp
                True, # AHU_east_fan
                False, # t1 
                True, # t2 
                True, # t3 
                False, # t4 
                False, # t5 
                False, # ChilledWaterSupplyTemp 
                True, # hvac_power  
            ]

class AHUModelObsWrapperMinimal(ObservationFilterWrapper):
    @staticmethod
    def filter():
        return [
                False, # DayOfYear 
                False, # CurrentTime 
                False, # OutdoorTemp 
                False, # OutdoorRelHum 
                False, # OutdoorDewTemp 
                False, # OutdoorTempWB 
                False, # OutdoorDiffuseSolar 
                False, # OutdoorDirectSolar 
                False, # OutdoorAzimuthAngle 
                False, # OutdoorAltitudeAngle 
                False, # AHU_east_temp
                False, # AHU_east_fan
                False, # t1 
                True, # t2 
                True, # t3 
                False, # t4 
                False, # t5 
                False, # ChilledWaterSupplyTemp 
                False, # hvac_power  
            ]