'''
This helps produce gym envs, with all the required wrapper. Move away from setting environment variables

Draws on the things found here baselines_energyplus/common/energyplus_util.py
'''
import os
import datetime
import gym
from pathlib import Path
import json

from gym_energyplus.envs.energyplus_env import EnergyPlusEnv
from gym_energyplus.wrappers import NormaliserWrapper, EarlyEndWrapper, VerifyActionShapeWrapper
from gym_energyplus.wrappers_basic_model_ahu import AHUModelObsWrapper, AHUModelRewardWrapper,  AHUModelObsWrapperMinimal
from gym_energyplus.wrappers_basic_model_ahu_supply_air_sp import AHUModelTempOnlyObsWrapper

class Factory:

    def __init__(self):
        config = json.load(open("./config.json")) 
        self.energyplus_file= config["eplus_abs_path"]

        
    def make_basic_model_no_rew(self, inst_log_dir: str):
        """

        WARNING: This model does not return a real reward - can not be used for learning
            - this was a first draft to show can interact with a simulation and apply control
        NOTE: It is expected that will be created in a 'with' block so it is guarenteed that clean up happens.
            e.g with basic_model() as env: 

        Args:
        inst_log_dir (str): the path that this instance should log to.
            NOTE: only energyplus logs are automatically logged
        """
        
        env = EnergyPlusEnv(
                
                energyplus_file= self.energyplus_file, 
                model_file= "./buildings/basic_model_ahu.idf",
                weather_file= "./weather/AUS_NSW.Sydney_IWEC.epw",
                log_dir=  inst_log_dir,
                verbose=False
            )
        env = NormaliserWrapper(env)
        env = VerifyActionShapeWrapper(env)
        env = gym.wrappers.ClipAction(env) 


        write_env_log(env.metadata, inst_log_dir)
        return env



    def make_basic_ahu(self, inst_log_dir: str):
        """
        NOTE: It is expected that will be created in a 'with' block so it is guarenteed that clean up happens.
            e.g with make_basic_ahu as env: 

        This is a single AHU (east), allowing control of the air temp and mass.

        Args:
        inst_log_dir (str): the path that this instance should log to.
            NOTE: only energyplus logs are automatically logged
        """

        env = EnergyPlusEnv(
                energyplus_file= self.energyplus_file, 
                model_file= "./buildings/basic_model_ahu.idf",
                weather_file= "./weather/AUS_NSW.Sydney_IWEC.epw",
                log_dir= inst_log_dir,
                verbose=False
            ) 
        env = AHUModelRewardWrapper(env)
        env = AHUModelObsWrapper(env)
        env = NormaliserWrapper(env)
        env = VerifyActionShapeWrapper(env)
        env = gym.wrappers.ClipAction(env)
        env.metadata.update({ "ClipActionWrapper": {"used": True}})

        write_env_log(env.metadata, inst_log_dir)
        return env

    def make_basic_model_ahu_supply_air_sp(self, inst_log_dir: str):

        env = EnergyPlusEnv(
                energyplus_file= self.energyplus_file, 
                model_file= "./buildings/basic_model_ahu_supply_air_sp.idf",
                weather_file= "./weather/AUS_NSW.Sydney_IWEC.epw",
                log_dir= inst_log_dir,
                verbose=False
            ) 
        env = AHUModelRewardWrapper(env)
        env = AHUModelTempOnlyObsWrapper(env) 
        env = NormaliserWrapper(env)
        env = VerifyActionShapeWrapper(env)
        env = gym.wrappers.ClipAction(env)
        env.metadata.update({ "ClipActionWrapper": {"used": True}})
        write_env_log(env.metadata, inst_log_dir)
        return env

    def make_single_day_basic_ahu_supply_air_sp(self, inst_log_dir: str):
        env = EnergyPlusEnv(
                energyplus_file= self.energyplus_file, 
                model_file= "./buildings/basic_model_ahu_supply_air_sp_single_day.idf",
                weather_file= "./weather/AUS_NSW.Sydney_IWEC.epw",
                log_dir= inst_log_dir,
                verbose=False
            ) 
        # TODO: this env should never apprear without this wrapper - is there a way to make this safer so its not forgotten?
        env = EarlyEndWrapper(env) # this is required to get proper reset behavior
        env = AHUModelRewardWrapper(env)
        env = AHUModelTempOnlyObsWrapper(env) 
        env = NormaliserWrapper(env)
        env = VerifyActionShapeWrapper(env)
        env = gym.wrappers.ClipAction(env)
        env.metadata.update({ "ClipActionWrapper": {"used": True}})
        write_env_log(env.metadata, inst_log_dir)
        return env


    @staticmethod
    def recommended_log_path(env_name: str, learner_name: str) -> str:
        '''
        This is a default/recommended logging name.

        Factory should be passed a path, as it does not fall within it's responsibility to determine a logging location
        That said - this is a suggested path format that could be used - to save the user having to remember it each time.
        '''
        return str(Path ("experiments", env_name,learner_name, datetime.datetime.now().strftime('openai-%Y-%m-%d-%H-%M-%S-%f')))


def write_env_log(data: dict, path: str):
    # safely creates the dir so that log file can be written
    path = Path(path)
    path.mkdir(parents = True, exist_ok = True)

    with open(path / "env_log.json", "w") as fp:
        json.dump(data,fp)