This folder contrains a number of important runs.

The purpose of each folder is as follows:
| Name | Description |
| ---  | ----------- |
|basic_ahu_supply_air_sp|information about fixed value control runs|
|examples|used to demonastrate many of the utils provided in the eval notebook|
|report|Contain the runs mentioned in the paper|
|single_day_basic_ahu_supply_air_ap|information about fixed value control runs|
