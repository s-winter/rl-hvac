import os
from baselines import logger
import datetime
from pathlib import Path
import logging
import numpy as np
import datetime

from gym_energyplus.env_factory import Factory

def train(env,s):
    setpoints = [1,1,-.2,-.2] * 50


    obs = env.reset()
    done = False
    i = 0
    while not done and i < 20000:
        # for action space of box, this can be an integer, otherwise should be np.array

        action = np.array([setpoints[i]], dtype = "float32")
        obs, rewards, done, info = env.step(action)
        i += 1
    print("done")
    env.close()


def main():
    s = 18 # the setpoint

    low =10
    high = 24
    s_norm = (s-low)/(high-low) * 2 -1


    factory = Factory()
    log_path = Path(Factory.recommended_log_path("single_day_basic_ahu_supply_air_sp", f"debug"))
    log_path.mkdir(parents=True, exist_ok= True)

    logging.basicConfig(filename= log_path / "learner_log.log", level=logging.DEBUG)
    logging.info(f"deterministic run of")

    with factory.make_single_day_basic_ahu_supply_air_sp(str(log_path)) as env:

        # logger.configure(str(log_path))
        # env = Monitor(env, logger.get_dir())
        env.seed(0)

        train(env, s_norm)



if __name__ == '__main__':
    start = datetime.datetime.now()
    main()
    end = datetime.datetime.now()
    print(f"total time: {end-start}")