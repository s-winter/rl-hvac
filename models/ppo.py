
import os
import datetime
from pathlib import Path
import random
import logging
import gym

from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.monitor import Monitor
from stable_baselines3 import PPO

from gym_energyplus.env_factory import Factory
from models.helper.eval import EvalCallbackSaveActions




def train(env, eval_env, num_timesteps, log_path):

    save_path = ""

    # Load an existing run
    shared_creation_params = dict(
        tensorboard_log= log_path,
        n_steps = 56 * 64, 
        batch_size = 56*8, # this is significantly larger than standard, but sill smaller than what the monash paper did
        learning_rate =3e-4
    )
    if save_path:
        ppo_load_params = dict(
            **shared_creation_params,
            path = save_path)
        
        logging.info(f"ppo_load_params: {ppo_load_params}")
        model = PPO.load(**ppo_load_params, env=env )
    else:
        ppo_creation_params = dict(
            **shared_creation_params,
            policy = 'MlpPolicy',
            verbose=1,
            policy_kwargs = dict(net_arch = [dict(pi=[32] * 2, vf=[32]*2)]), 
            seed = random.randint(1,10_000),
            ent_coef = 0.0
            )
        logging.info(ppo_creation_params)
        model = PPO(**ppo_creation_params, env = env)

    
    eval_callback_params = dict(
            # This is where the EvalCallBack will save a model that has the best mean ep rew so far - will continually overwrite to jsut save the best one
            best_model_save_path=log_path / "best_model",
            log_path=log_path / "best_model",
            # (can go in train) how many steps between eval. Doesn't make sense to eval more that once per rollout. eval_freq = x * n_steps -1
            # note x should be chose relative to n_steps -> determines how much of the time is spent evaluative vs. improving. 56/n_step = evaluation time
            eval_freq= 2 * model.n_steps - 1,
            n_eval_episodes=1, # env is deterministic so one ep is enough
            min_eval_ep_len = 56,
            )
    logging.info(eval_callback_params)       
    # build custom callback for additional details when logging
    eval_callback = EvalCallbackSaveActions(
                eval_env = eval_env,
                **eval_callback_params
            )


    ppo_run_params = dict(
        # total number of timestep that can be taken (this budget is consumed by the rollout part)
        total_timesteps = num_timesteps, 
        # How many times want to do a complete rollout before reporting rollout behaviour (this gives exploration information)
        log_interval = 1,
        tb_log_name = "tensor_board",
        callback = eval_callback
        )
    logging.info(ppo_run_params)
    model.learn(**ppo_run_params)
    model.save(log_path / "end_model.zip")
    logging.info("done")



def main():
    factory = Factory()
    log_path = Path(Factory.recommended_log_path("single_day_basic_ahu_supply_air_sp", "ppo"))
    log_path.mkdir(parents=True, exist_ok= True)
    logging.basicConfig(filename= log_path / "learner.log", level = logging.DEBUG)
    logging.info("Standard Learner")

    with factory.make_single_day_basic_ahu_supply_air_sp(str(log_path)) as env:    
            
            # Allow early resets should really be false - especially for the eval_model
                # Looks like there is a bug in the eval that means when using a vec env the resets of the monitor get messed up
            env = Monitor(env, allow_early_resets= True)
            env.seed(0)
            env = DummyVecEnv([lambda: env])

            train(env,env, num_timesteps= 1_500_000, log_path = log_path)



if __name__ == '__main__':
    main()



