'''
This file contains helpers to evluate SB3 learners. The callback are based on those provided in SB3, and provide additional information
'''


from typing import Any, Callable, Dict, List, Optional, Union
import numpy as np
import os

import gym
from stable_baselines3.common import base_class 
from stable_baselines3.common.vec_env import DummyVecEnv, VecEnv, sync_envs_normalization
# Looks like we might need our own callback to collect the reward info throughough the evaluation runs
from stable_baselines3.common.callbacks import EvalCallback, BaseCallback
from stable_baselines3.common.evaluation import evaluate_policy

class EvalCallbackSaveActions(EvalCallback):
    '''
    Almost identical to the standard EvalCallback, except it records and saves the actions take by the agent when it is evaluated 
        - (add evaluations_step_action and evaluations_step_reward attributes)
    There are some restrictions placed on the standard input so that it is more obvious how to use this function in out context.

    How to instanciate this callback:
        - eval_env: pass a reference to the standard env here. This does not need to be a different env to the training one.
        - eval_freq: should be set as normal
        - log_path/best_model_path: In standard instatiation these are same. log_path is where npz saved. best_model is where model zip saved
        - Other params: can be left as default

    Args:
        min_eval_ep_len: Define the minimum number of steps expected in the env during evaluation to be considered valid run.
        Only save resulting model if it has been run for more than that. See note below for details. 

    NOTE: min_eval_ep_len. Need to ensure that the right amount of evalution has been run for a suitable number of steps. There are 2 contributing factors:
        - BUG: Needs to be wrapped in monitor wrapper that ALLOWS early resets.
            There is a bug in evaluation_policy that results in double resets because of the use of a VecEnv.
          Ideally would not allow early resets
        - BUG: There appears to be an early reset happening withing the environment/simulation at some point. Result is only one step. Under investigate.
            Until this is resolved recommend using min_eval_ep_len, other wise could store a best model that has been run for only 1 step
    '''

    def __init__(
        self,
        eval_env: Union[gym.Env, VecEnv],
        n_eval_episodes: int = 1,
        eval_freq: int = 10000,
        log_path: Optional[str] = None,
        best_model_save_path: Optional[str] = None,
        deterministic: bool = True,
        render: bool = False,
        verbose: int = 1,
        warn: bool = True,
        min_eval_ep_len = 0 #THIS PARAMETER IS NEW
    ):
        assert n_eval_episodes==1, f"Callback is only designed for a single eval ep. This requires n_eval_episodes==1, found n_eval_episodes=={n_eval_episodes}"

        super(EvalCallbackSaveActions, self).__init__(
            eval_env,
            callback_on_new_best = None,
            n_eval_episodes = n_eval_episodes,
            eval_freq =eval_freq,
            log_path =log_path,
            best_model_save_path =best_model_save_path,
            deterministic =deterministic,
            render =render,
            verbose =verbose,
            warn =warn
        )
        
        self.min_eval_ep_len = min_eval_ep_len
        self.evaluations_step_actions: List[np.ndarray] = []
        self.evaluations_step_rewards: List[np.ndarray]  = []
        self.ep_action = []
        self.ep_rewards = []

    def _collect_step_action_and_reward(self, locals_: Dict[str, Any], globals_: Dict[str, Any]) -> None:
        '''
        For each step record the action and reward recieved
        '''
        # Actions: for each row of observation returns an action. [[action_for_1_ob]]
        self.ep_action.append(locals_["actions"][0])
        self.ep_rewards.append(locals_["reward"])
        # need to clear the ep_specific reward/actions
        if locals_["done"]:
            self.evaluations_step_actions.append(np.array(self.ep_action))
            self.evaluations_step_rewards.append(np.array(self.ep_rewards))
            self.ep_action = []
            self.ep_rewards = []


    def _on_step(self) -> bool:

        if self.eval_freq > 0 and self.n_calls % self.eval_freq == 0:
            # Sync training and eval env if there is VecNormalize
            sync_envs_normalization(self.training_env, self.eval_env)

            # Reset success rate buffer
            self._is_success_buffer = []

            episode_rewards, episode_lengths = evaluate_policy(
                self.model,
                self.eval_env,
                n_eval_episodes=self.n_eval_episodes,
                render=self.render,
                deterministic=self.deterministic,
                return_episode_rewards=True,
                warn=self.warn,
                callback=self._collect_step_action_and_reward, #THIS LINE HAS BEEN CHANGED
            )

            if self.log_path is not None:
                self.evaluations_timesteps.append(self.num_timesteps)
                self.evaluations_results.append(episode_rewards)
                self.evaluations_length.append(episode_lengths)

                kwargs = {}
                # Save success log if present
                if len(self._is_success_buffer) > 0:
                    self.evaluations_successes.append(self._is_success_buffer)
                    kwargs = dict(successes=self.evaluations_successes)

                #### THIS SECTION HAS BEEN CHANGED ###
                np.savez(
                    self.log_path,
                    timesteps=self.evaluations_timesteps,
                    results=self.evaluations_results,
                    ep_lengths=self.evaluations_length,
                    evaluations_step_actions=self.evaluations_step_actions,
                    evaluations_step_rewards=self.evaluations_step_rewards,
                    **kwargs,
                )
                ###### END CHANGED SECTION ######

            mean_reward, std_reward = np.mean(episode_rewards), np.std(episode_rewards)
            mean_ep_length, std_ep_length = np.mean(episode_lengths), np.std(episode_lengths)
            self.last_mean_reward = mean_reward

            if self.verbose > 0:
                print(f"Eval num_timesteps={self.num_timesteps}, " f"episode_reward={mean_reward:.2f} +/- {std_reward:.2f}")
                print(f"Episode length: {mean_ep_length:.2f} +/- {std_ep_length:.2f}")
            # Add to current Logger
            self.logger.record("eval/mean_reward", float(mean_reward))
            self.logger.record("eval/mean_ep_length", mean_ep_length)

            if len(self._is_success_buffer) > 0:
                success_rate = np.mean(self._is_success_buffer)
                if self.verbose > 0:
                    print(f"Success rate: {100 * success_rate:.2f}%")
                self.logger.record("eval/success_rate", success_rate)

            # Dump log so the evaluation results are printed with the correct timestep
            self.logger.record("time/total timesteps", self.num_timesteps, exclude="tensorboard")
            self.logger.dump(self.num_timesteps)

            # Due to the bug described at the beginning of the call back, need to check that the environement was evaluated to completetion
            if (mean_reward > self.best_mean_reward) and (mean_ep_length >= self.min_eval_ep_len):
                if self.verbose > 0:
                    print("New best mean reward!")
                if self.best_model_save_path is not None:
                    self.model.save(os.path.join(self.best_model_save_path, "best_model"))
                self.best_mean_reward = mean_reward
                # Trigger callback if needed
                if self.callback is not None:
                    return self._on_event()

        return True