# Project Description
Reinforcement Learning Testbed for Power Consumption Optimization. This repo builds on the work produced here https://github.com/IBM/rl-testbed-for-energyplus (This is refered to as the IBM framework). The intention is to present an interpretable toy example that can be used as a sandbox before moving onto more realistic environment, such as those found https://github.com/bsl546/energym or https://github.com/ibpsa/project1-boptest-gym. 


## Supported platforms
The extensions presented here have been tested on Ubuntu 20.04, and EnergyPlus 9.3.0. It should be noted that the original work does not support Windows, and niether does this repo.

## Installation
Installaton of rl-hvac consists of three parts. This is predominantly the same as those found in the IBM framework, with some minor additions:

- Install EnergyPlus prebuild package
- Build patched EnergyPlus
- Install built executables

### Install EnergyPlus prebuilt package
First, download pre-built package of EnergyPlus and install it.
This is not for executing normal version of EnergyPlus, but to get some pre-compiled binaries and data files that can not be generated from source code.

Supported EnergyPlus versions:

|       | Linux                                                                                                                                                  | MacOS                                                                                                                                                      |
|-------|--------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 8.8.0 | [EnergyPlus-8.8.0-7c3bbe4830-Linux-x86_64.sh](https://github.com/NREL/EnergyPlus/releases/download/v8.8.0/EnergyPlus-8.8.0-7c3bbe4830-Linux-x86_64.sh) | [EnergyPlus-8.8.0-7c3bbe4830-Darwin-x86_64.dmg](https://github.com/NREL/EnergyPlus/releases/download/v8.8.0/EnergyPlus-8.8.0-7c3bbe4830-Darwin-x86_64.dmg) |
| 9.1.0 | [EnergyPlus-9.1.0-08d2e308bb-Linux-x86_64.sh](https://github.com/NREL/EnergyPlus/releases/download/v9.1.0/EnergyPlus-9.1.0-08d2e308bb-Linux-x86_64.sh) | [EnergyPlus-9.1.0-08d2e308bb-Darwin-x86_64.dmg](https://github.com/NREL/EnergyPlus/releases/download/v9.1.0/EnergyPlus-9.1.0-08d2e308bb-Darwin-x86_64.dmg) |
| 9.2.0 | [EnergyPlus-9.2.0-921312fa1d-Linux-x86_64.sh](https://github.com/NREL/EnergyPlus/releases/download/v9.2.0/EnergyPlus-9.2.0-921312fa1d-Linux-x86_64.sh) | [EnergyPlus-9.2.0-921312fa1d-Darwin-x86_64.dmg](https://github.com/NREL/EnergyPlus/releases/download/v9.2.0/EnergyPlus-9.2.0-921312fa1d-Darwin-x86_64.dmg) |
| 9.3.0 | [EnergyPlus-9.3.0-baff08990c-Linux-x86_64.sh](https://github.com/NREL/EnergyPlus/releases/download/v9.3.0/EnergyPlus-9.3.0-baff08990c-Linux-x86_64.sh) | [EnergyPlus-9.3.0-baff08990c-Darwin-x86_64.dmg](https://github.com/NREL/EnergyPlus/releases/download/v9.3.0/EnergyPlus-9.3.0-baff08990c-Darwin-x86_64.dmg) |

You can also download the installer at https://github.com/NREL/EnergyPlus/releases/.

#### Ubuntu

1. Go to the web page shown above.
2. Right click on relevant link in supported versions table and select `Save link As` to from the menu to download installation image.
3. (9.1.0, Linux only) Apply patch on downloaded file (EnergyPlus 9.1.0 installation script unpacks in /usr/local instead of /usr/local/EnergyPlus-9.1.0).
```
$ cd <DOWNLOAD-DIRECTORY>
$ patch -p0 < rl-hvac/EnergyPlus/EnergyPlus-9.1.0-08d2e308bb-Linux-x86_64.sh.patch
```
4. Execute installation image. Below example is for EnergyPlus 9.1.0
```
$ sudo bash <DOWNLOAD-DIRECTORY>/EnergyPlus-9.1.0-08d2e308bb-Linux-x86_64.sh
```

Enter your admin password if required.
Specify `/usr/local` for install directory.
Respond with `/usr/local/bin` if asked for symbolic link location.
The package will be installed at `/usr/local/EnergyPlus-<EPLUS_VERSION>`.

#### macOS

1. Go to the web page shown above.
2. Right click in supported versions table and select `Save link As` to from the menu to download installation image.
3. Double click the downloaded package, and follow the instructions.
The package will be installed in `/Applications/EnergyPlus-<EPLUS_VERSION>`.

### Build patched EnergyPlus

Download source code of EnergyPlus and rl-testbed-for-energyplus. In below scripted lines, replace `<EPLUS_VERSION>`
by the one you're using (for instance, `9.3.0`)

```
$ cd <WORKING-DIRECTORY>
$ git clone --depth 1 --branch v<EPLUS_VERSION>  https://github.com/NREL/EnergyPlus.git
```

Apply patch to EnergyPlus and build. Replace `<EPLUS_VERSION>`
by the one you're using (for instance, `9-3-0`). 

NOTE: these patches come from the IBM framework. An adjusted version has been included in this repo as there appears to be a spelling mistake. This is being raised, and instruction will be adjust once this has been resolved.

```
$ cd <WORKING-DIRECTORY>/EnergyPlus
$ patch -p1 < ../rl-hvac/EnergyPlus/RL-patch-for-EnergyPlus-<EPLUS_VERSION>.patch
$ mkdir build
$ cd build
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local/EnergyPlus-<EPLUS_VERSION> ..    # Ubuntu case (please don't forget the two dots at the end)
$ cmake -DCMAKE_INSTALL_PREFIX=/Applications/EnergyPlus-<EPLUS_VERSION> .. # macOS case (please don't forget the two dots at the end)
$ make -j4
```
Note for Ubuntu users:
If the cmake command above returns the following error "No CMAKE_CXX_COMPILER could be found", install g++ and re-run.

### Install built executables
```
$ sudo make install
```

### Update the Config
In the config.json file in the root of this repo update the value associated with `eplus_abs_path` to be the path to the EnergyPlus installation. This is the path specifed in the energyplus prebuilt install e.g `/usr/local/EnergyPlus-<EPLUS_VERSION>/energyplus`.

### Install the requirements

```
$ pip install -r requirements.txt
```
### Define module
This allows the different modules to be imported by the notebooks found in eval. `PYTHONPATH` is used as this repo is currently under development and is not yet ready to be made into an installable Python package. Instructions for adding this on Unix are provided below. Additional instruction can be found here https://bic-berkeley.github.io/psych-214-fall-2016/using_pythonpath.html.

1. Open the file `~/.bashrc` with your text editor  (e.g `nano ~/.bashrc`).
2. Add the following at the end of the file `export PYTHONPATH=<PATH_TO_THIS_DIRECTORY>` where `<PATH_TO_THIS_DIRECTORY>` gives the path to the root of this repo (e.g home/Documents/rl-hvac).
3. Close and reopen the terminal.
4. Confirm it has been added by runiing `echo $PYTHONPATH`, which should print your `<PATH_TO_THIS_DIRECTORY>`.

### How to run
All commands should be run from the root of this repo.

#### Tests
There are a range of test suites included. This can be run with `pytest .`.

#### Learners
There are a number of learner that can be found within `./models`. Calling these scripts results in an experiment directory being created for this run, and a learner being created, trained and saved

### Monitoring

Tensorboard logs are generated during training of models. It is recommened this is used for monitoring purposes. Additionally there are a number of utils for post-run evaluation. Please see "eval" section for further details on these.

## License <a name="license"></a>
The Reinforcement Learning for hvac uses the [MIT License](LICENSE) software license.

## How to cite
For citing the use or extension of this testbed, please cite the original testbed (https://github.com/IBM/rl-testbed-for-energyplus)


## Related information
- Original work the repo is based on https://github.com/IBM/rl-testbed-for-energyplus
- EnergyPlus: https://github.com/NREL/EnergyPlus
- OpenAI Gym: https://github.com/OpenAI/gym
- StableBaselines3: https://stable-baselines3.readthedocs.io/en/master/
